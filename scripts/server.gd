extends Node2D

const PORT        = 34534
const MAX_PLAYERS = 200

func _ready():
    var server = NetworkedMultiplayerENet.new()
    server.create_server(PORT, MAX_PLAYERS)
    get_tree().set_network_peer(server)

extends Node

var players: Dictionary = {}

func add_player(player_id, player):
    players[player_id] = player

func remove_player(player_id):
    players.erase(player_id) 

func _ready():
    var test_player = preload("res://tests/test_player.gd")
    add_child(test_player.new())

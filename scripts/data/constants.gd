const PLAYER_INIT_SPEED = 1.0
const PLAYER_INIT_MAX_SIZE = 100
const PLAYER_INIT_BODY = Vector2(0.0, 10.0)

const HEAD_COLLISION_CLAMP = 1.0

const META_OBJ_PLAYER = "obj_player"
const META_IS_HEAD = "is_head"
const META_REFERENCE_HEAD = "head_reference"

const HEAD_RANGE_WIDTH = 0.125
const HEAD_RANGE_DISTANCE = 15.0
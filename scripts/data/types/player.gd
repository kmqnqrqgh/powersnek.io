extends Node
class_name Player_

const Body = preload("res://scripts/data/types/body.gd")
const Constants = preload("res://scripts/data/constants.gd")

# todo: add const max speed

# list of Vector2D for each "corner" of the snake
export var body_points: Array

# "pointer" to the first moving part of the snake 
export var head: Vector2

# distance to add on each frame when moving
export var speed: float

# current limit of size for the snake
export var max_length: float

# used for collisions. keeps track of each collision, by its coordinates
# fails to add collision if coordinates already exists, thus we must check if it worked
var collision: Body

enum Directions {
    up, down, left, right
}

# array of Directions. it must never be empty, as the only state for the current
# direction is the only remaining item. always
# user might send many commands at once, we must process all of them
var next_directions: Array = [Directions.up]

func _init(initial_position: Vector2) -> void:
    body_points         = [Constants.PLAYER_INIT_BODY + initial_position]
    head                = initial_position
    speed               = Constants.PLAYER_INIT_SPEED
    max_length          = Constants.PLAYER_INIT_MAX_SIZE
    collision           = Body.new(self, initial_position)
    add_child(collision)
    collision.add(body_points.back(), head)

# call this inside _physics_process
func update() -> void:
    if next_directions.size() > 1:
        # store old direction
        var old_direction = next_directions.pop_front()
        # old first item is gone, now the first item is the new direction
        __update_direction(old_direction, next_directions.front())
    else:
        # same direction
        __update_direction(next_directions.front(), next_directions.front())

func request_move_up() -> void:
    if next_directions.back() != Directions.down:
        next_directions.append(Directions.up)

func request_move_down() -> void:
    if next_directions.back() != Directions.up:
        next_directions.append(Directions.down)

func request_move_left() -> void:
    if next_directions.back() != Directions.right:
        next_directions.append(Directions.left)
    
func request_move_right() -> void:
    if next_directions.back() != Directions.left:
        next_directions.append(Directions.right)

func __update_direction(direction, new_direction) -> void:
    if direction != new_direction:
        direction = new_direction
        # new collisoin start at a single point
        if not collision.add(head, head):
            return
        body_points.append(head)
    
    # bugfix: changing directions increases snake size
    if length() < max_length:
        __move_head(direction)
    __move_tail()

func __move_head(direction) -> void:
    var old_head = head
    # faster than using keyword match
    if direction == Directions.up:
        head.y -= speed
    elif direction == Directions.down:
        head.y += speed
    elif direction == Directions.left:
        head.x -= speed
    elif direction == Directions.right:
        head.x += speed
    collision.update_collision(body_points.back(), old_head,
                               body_points.back(), head, true)

# must be called after __move_head because head collision is updated there
func __move_tail() -> void:
    if length() >= max_length:
        if body_points.size() > 1: 
            var tail = [body_points[0], body_points[1]]
            # when the tail reaches the first curve
            if body_points[0] == body_points[1]:
                # remove collision
                collision.remove(tail[0], tail[1])
                # and remove the tail point
                body_points.pop_front()
            else:
                # automatically gets the direction and move it with the current speed
                body_points[0] += (body_points[1] - body_points[0]).clamped(speed)
                # old_start, old_end, new_start, new_end
                collision.update_collision(tail[0], tail[1], 
                                           body_points[0], body_points[1])
        else:
            var old_tail = body_points[0]
            body_points[0] += (head - body_points[0]).clamped(speed)
            collision.update_collision(old_tail, head, body_points[0], head)

# calculate snake length precisely
func length() -> float:
    var point_distance = 0
    if body_points.size() < 2:
        point_distance = head - body_points.back()
        return abs(point_distance.x) + abs(point_distance.y)
    var sum: float = 0
    var previous_point = body_points[0]
    for point in body_points:
        point_distance = point - previous_point
        sum += abs(point_distance.x) + abs(point_distance.y)
        previous_point = point
    point_distance = head - body_points.back()
    sum += abs(point_distance.x) + abs(point_distance.y)
    return sum
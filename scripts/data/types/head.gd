extends Node

const Constants = preload("res://scripts/data/constants.gd")

# hitbox for the head 
var collision: Area2D

# used to apply boost when close to other player's bodies 
var proximity: Area2D

# array of other players close to this head 
var close_players = []

# for future reference, for killing the player.
# i could get the node parent but this is more reliable
var player # type = Player

# when the head collides to another player
signal head_collision(player, is_mutual)

# reports the closest player plus where its directioned relative to this head
signal head_proximity(level, direction)
# todo: connect these signals in player.gd

# when the head collides to a wall
signal wall_collision

func _init():
    create_head_collision()
    create_head_range_field()

# head must be ahead of the body to not collide with itself when turning
func create_head_collision():
    collision = Area2D.new()
    var rect = RectangleShape2D.new()
    # must use half of the clamp to avoid collisions on curves
    var size = Constants.HEAD_COLLISION_CLAMP / 2
    rect.extents = Vector2(size, size)
    var owner_id = collision.create_shape_owner(rect)
    collision.shape_owner_add_shape(owner_id, rect)
    collision.connect("area_entered", self, "on_head_collision")
    add_child(collision)

func create_head_range_field():
    proximity = Area2D.new()
    # create a cross, so i dont need to keep track of the head alignment
    var rect1 = RectangleShape2D.new()
    var rect2 = RectangleShape2D.new()
    var vec = Vector2(Constants.HEAD_RANGE_WIDTH, Constants.HEAD_RANGE_DISTANCE)
    rect1.extents = vec
    rect2.extents.x = vec.y
    rect2.extents.y = vec.x
    proximity.shape_owner_add_shape(proximity.create_shape_owner(rect1), rect1)
    proximity.shape_owner_add_shape(proximity.create_shape_owner(rect2), rect2)
    proximity.connect("area_entered", self, "on_head_proximity")
    proximity.connect("area_exited", self, "on_head_diversion")

func get_closest_player_head(area: Area2D):
    # todo: implement this 
    pass

func on_head_collision(area: Area2D) -> void:
    var obj_player = area.get_meta(Constants.META_OBJ_PLAYER)
    if obj_player == null:
        emit_signal("wall_collision")
    var mutual = false
    # get special treatment when two heads collide
    if area.get_meta(Constants.META_IS_HEAD):
        mutual= true
    emit_signal("head_collision", obj_player)

func on_head_proximity(area: Area2D) -> void:
    # ignore ourselves
    var obj_player = area.get_meta(Constants.META_OBJ_PLAYER)
    if obj_player == self.player:
        print("ignored myself on_head_proximity")
        return

func on_head_diversion(area: Area2D) -> void:
    # ignore ourselves
    var obj_player = area.get_meta(Constants.META_OBJ_PLAYER)
    if obj_player == self.player:
        print("ignored myself on_head_diversion")
        return
extends Node

const Constants = preload("res://scripts/data/constants.gd")
const Head = preload("res://scripts/data/types/head.gd")

# store the Area2Ds in here.
var collision_areas: Dictionary

# this is what actually kills the player when running over another player's body
# or his own body
var body_head: Head

# for future reference, for killing the player.
# i could get the node parent but this is more reliable
var player # type = Player

signal external_collision(node)

func _init(player, init_pos):
    self.player = player
    collision_areas = {}
    body_head = Head.new()
    body_head.position = Constants.PLAYER_INIT_BODY + init_pos

func add(start: Vector2, end: Vector2) -> bool:
    """
    Adds new collision segment, which consists on a Area2D with a SegmentShape2D
    No CollisionShape2D are being used in order to minimize memory footprint
    """
    if collision_areas.get([start, end]):
        prints("collision segment already exists:", start, end)
        return false
    # must use this, other types dont work or dont have the necessary functionalities 
    var collision = Area2D.new()
    var segment = SegmentShape2D.new()
    segment.a = start
    segment.b = end
    var shape_id = collision.create_shape_owner(segment)
    collision.shape_owner_add_shape(shape_id, segment)
    collision.set_meta(Constants.META_OBJ_PLAYER, player)
    collision.connect("area_entered", self, "on_collision")
    collision_areas[[start, end]] = collision
    add_child(collision)
    return true

func remove(start: Vector2, end: Vector2) -> void:
    """
    Removes a collision segment
    """
    var collision: Area2D = collision_areas.get([start, end])
    if not collision:
        prints("error removing segment:", start, end)
        return
    # avoid memory leaks
    collision.queue_free()
    collision_areas.erase([start, end])

func update_collision(old_start: Vector2, old_end: Vector2,
                      new_start: Vector2, new_end: Vector2, is_head=false) -> void:
    """
    Updates a collision segment position and triggers the Area2D monitoring event
    in case of snakes colliding
    Use is_head=true when updating only the head
    """
    var collision: Area2D = collision_areas[[old_start, old_end]]
    var shape: SegmentShape2D = collision.shape_owner_get_owner(0)
    shape.a = new_start
    shape.b = new_end 
    # ignore if no change was made
    if old_end == new_end and old_start == new_start:
        return
    collision_areas[[new_start, new_end]] = collision
    collision_areas.erase([old_start, old_end])
    # bugfix: gotta touch Area2D in order to trigger the monitoring area event
    collision.position = collision.position
    if is_head:
        body_head.position = new_end + (
            new_end - old_end).clamped(Constants.HEAD_COLLISION_CLAMP)

func on_collision(area: Area2D) -> void:
    var obj = area.get_meta(Constants.META_OBJ_PLAYER)
    if obj == null:
        # hit a wall, the head must've triggered this signal already
        pass
    if obj != self:
        emit_signal("external_collision", obj)

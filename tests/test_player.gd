extends Node2D

const Player = preload("res://scripts/data/types/player.gd")
var player_inst: Player

func _ready():
    player_inst = Player.new(Vector2(200, 200))
    add_child(player_inst)
    position = Vector2(0,0)

func _physics_process(delta):
    player_inst.update()

func _draw():
    for point in player_inst.collision.collision_areas.values():
        var shape = point.shape_owner_get_owner(0)
        draw_line(shape.a, shape.b,Color.black,2.0)

func _process(delta):
    self.update()

func _input(event):
    if Input.is_action_just_pressed("ui_right"):
        player_inst.request_move_right()
    if Input.is_action_just_pressed("ui_left"):
        player_inst.request_move_left()
    if Input.is_action_just_pressed("ui_down"):
        player_inst.request_move_down()
    if Input.is_action_just_pressed("ui_up"):
        player_inst.request_move_up()
    if Input.is_action_just_pressed("ui_page_up"):
        player_inst.speed += 1
    if Input.is_action_just_pressed("ui_page_down"):
        if player_inst.speed >= 1:
            player_inst.speed -= 1